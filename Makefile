CXX = g++
CXX_FLAGS = -std=c++14 -O2 -pipe -Wall

trimitplease: *.cpp 
	$(CXX) $(CXX_FLAGS) -c *.cpp
	$(CXX) -o trimitplease *.o

.PHONY: test clean
test: trimitplease
	./trimitplease
clean:
	rm -f trimitplease *.o
