#include "trimmer.h"

int main(int argc, char* argv[]){
    Trimmer* t = new Trimmer;
    if(argc>1) for(int i=1;i<argc;i++) t->run(argv[i]);
    delete t;
    return 0;
}
