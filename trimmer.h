#ifndef TRIMMER_H
#define TRIMMER_H

#include <iostream>
#include <fstream>
#include <vector>

class Trimmer
{
public:
    Trimmer();
    int run(char* arg);

private:
    //members
    int copying;
    std::ifstream target;
    std::vector<std::string> blockBeginIndicators;
    std::vector<std::string> blockEndIndicators;
    std::vector<std::string> supportedExtensions;
    //helpers
    int switchNames(std::string s);
    int deleteExcessiveFile(std::string s);
    int checkPhrase(std::string s, std::vector<std::string> indicators);
    //main operations
    int load(std::string s);
    int trim(std::ifstream& target,std::string s);
    int close();
};
#endif // TRIMMER_H
