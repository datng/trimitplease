# Motivation:
While making graphs for another project I discovered that LibreOffice Draw's flat xml files (myrandomgraph.fodg) change even if I don't change anything in the graph itself. It turns out, that the program saves too much unwanted (well, unwanted *for me*) information - date of modification, viewport information, even the printer device settings in my computer. Not only does it make the files distracting in group projects while doing diffs, but also violates the privacy of the user, with no option to turn off. So I made this small program to trim all those parts away.

# Tested Formats:
### LibreOffice 5.4.3.2
* *.fodg
* *.fodt

# Compilation:
We currently use 2 equivalent methods:
* Using Qt Creator for trimitplease.pro
* Using make

# Todos:
* Make a GUI to use with the current mode
* Make the program runs without having to know the filenames beforehand, aka: start, ask the user for files to trim, trim, close
* Make a batch function to trim all files in a given directory
