#include "trimmer.h"

Trimmer::Trimmer(){
    copying = 1;
    blockBeginIndicators = {
        "<office:meta>"
        ,"<office:settings>"
        ,"<office:scripts>"
    };

    blockEndIndicators = {
        "</office:meta>"
        ,"</office:settings>"
        ,"</office:scripts>"
    };

    supportedExtensions = {
        ".fodg"
        ,".fodt"
    };
}

int Trimmer::run(char* arg){
    int err=0;
    std::string filename = std::string(arg);
    err=this->load(filename);
    if(err==0) err = this->trim(this->target, filename);
    this->close();
    return 0;
}

int Trimmer::checkPhrase(std::string s, std::vector<std::string> indicators){
    for(std::string phrase : indicators)
        if(s.find(phrase)!=std::string::npos) return 1;
    return 0;
}

int Trimmer::load(std::string s){
    std::cout<< "Opening file "<<s<<std::endl;
    if(!checkPhrase(s,supportedExtensions)){
        std::cout<<"  File type not supported"<<std::endl<<std::endl;
        return -1;
    }
    target.open(s);
    if(!target.is_open()){
        std::cout<<"  Error"<<std::endl<<std::endl;
        return -1;
    }
    return 0;
}

int Trimmer::trim(std::ifstream& target,std::string s){
    std::ofstream trimmed;
    trimmed.open("trimmed_"+s);
    if(!trimmed.is_open()){
        std::cout<<"Error: cannot write file "<<"trimmed_"+s<<std::endl;
        return -1;
    }

    std::string line = "non-empty line";
    std::cout<<"  Trimming... "<<std::endl;
    int linesTrimmed = 0;
    while(!target.eof()){
        std::getline(target,line);
        if(checkPhrase(line,blockBeginIndicators)) copying = 0;
        if(copying){
            if(line=="</office:document>")trimmed<<line;
            else trimmed<<line<<std::endl;
        } else linesTrimmed++;
        if(!copying && checkPhrase(line,blockEndIndicators)) copying =1;
    }
    if(!linesTrimmed){
        std::cout<<"  No trimming neccessary"<<std::endl<<std::endl;
        deleteExcessiveFile(s);
    }
    else {
        std::cout<<"  Done"<<std::endl<<std::endl;
        switchNames(s);
    }

    trimmed.close();
    return 0;
}

int Trimmer::deleteExcessiveFile(std::string s){
    std::string trimmed_prefix = "trimmed_"+s;
    std::remove(trimmed_prefix.data());
    return 0;
}

int Trimmer::switchNames(std::string s){
    std::string original_prefix = "original_"+s;
    std::string trimmed_prefix = "trimmed_"+s;
    std::rename(s.data(),original_prefix.data());
    std::rename(trimmed_prefix.data(),s.data());
    return 0;
}

int Trimmer::close(){
    target.close();
    return 0;
}
